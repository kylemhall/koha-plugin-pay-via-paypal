/*
	[%
		TOKENS = {
			home_breadcrumb = "Kezdőlap"
			your_payment_breadcrumb = "Your Payment"
			error = "Hiba: "
			error_title = "Valami hiba van az üzenetével"
			PAYPAL_UNABLE_TO_CONNECT = "Unable to connect to PayPal."
			PAYPAL_TRY_LATER = "Kérjük, próbálkozzon újra később."
			PAYPAL_ERROR_PROCESSING = "Unable to verify payment."
			PAYPAL_CONTACT_LIBRARY = "Másodlagos elérhetõségi információk"
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Adja vissza ezt a dokumentumot "
		}
	%]
*/