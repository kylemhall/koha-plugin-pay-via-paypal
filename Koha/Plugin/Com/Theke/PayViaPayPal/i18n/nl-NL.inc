/*
	[%
		TOKENS = {
			home_breadcrumb = "Start"
			your_payment_breadcrumb = "Your Payment"
			error = "Fout:"
			error_title = "Er is een probleem met uw bijdrage"
			PAYPAL_UNABLE_TO_CONNECT = "Het is niet gelukt om tags toe te voegen."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "Het is niet gelukt om tags toe te voegen."
			PAYPAL_CONTACT_LIBRARY = ". Neem a.u.b. contact op met de bibliotheek voor meer informatie."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Terugbezorgen naar "
		}
	%]
*/