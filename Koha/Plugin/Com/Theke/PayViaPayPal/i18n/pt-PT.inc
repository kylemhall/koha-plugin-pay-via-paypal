/*
	[%
		TOKENS = {
			home_breadcrumb = "Início"
			your_payment_breadcrumb = "Your Payment"
			error = "Erro"
			error_title = "houve um problema ao processar o seu pedido"
			PAYPAL_UNABLE_TO_CONNECT = "Não foi possível conectar ao PayPal."
			PAYPAL_TRY_LATER = "Por favor tente novamente mais tarde."
			PAYPAL_ERROR_PROCESSING = "Não foi possível verificar o pagamento."
			PAYPAL_CONTACT_LIBRARY = "Por favor contacte a biblioteca para verificar o seu pagamento."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Voltar aos detalhes da multa"
		}
	%]
*/