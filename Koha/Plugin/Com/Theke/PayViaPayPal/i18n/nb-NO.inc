/*
	[%
		TOKENS = {
			home_breadcrumb = "Hjem"
			your_payment_breadcrumb = "Your Payment"
			error = "Feil"
			error_title = "det oppsto et problem med betalingen din"
			PAYPAL_UNABLE_TO_CONNECT = "Kunne ikke koble til PayPal."
			PAYPAL_TRY_LATER = "Prøv igjen senere."
			PAYPAL_ERROR_PROCESSING = "Kunne ikke verifisere betaling."
			PAYPAL_CONTACT_LIBRARY = "Kontakt biblioteket for å verifisere betalingen."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Gå tilbake til gebyrdetaljer"
		}
	%]
*/