/*
	[%
		TOKENS = {
			home_breadcrumb = "Home"
			your_payment_breadcrumb = "Your Payment"
			error = "Kesalahan:"
			error_title = "Ada masalah dengan pengiriman anda"
			PAYPAL_UNABLE_TO_CONNECT = "Tidak dapat menambah satu atau lebih tag."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "Tidak dapat menambah satu atau lebih tag."
			PAYPAL_CONTACT_LIBRARY = ". Mohon hubungi perpustakaan untuk informasi lebih lanjut."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Kembalikan barang ini "
		}
	%]
*/