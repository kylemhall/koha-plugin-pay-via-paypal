/*
	[%
		TOKENS = {
			home_breadcrumb = "Home"
			your_payment_breadcrumb = "Your Payment"
			error = "Eroare:"
			error_title = "Nu a fost o problemă cu depunerea ta"
			PAYPAL_UNABLE_TO_CONNECT = "Nu se poate conecta la PayPal."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "Nu se poate verifica plata."
			PAYPAL_CONTACT_LIBRARY = "Indormații de contact alternative"
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Returnare a cestiu articol "
		}
	%]
*/