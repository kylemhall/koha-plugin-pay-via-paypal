/*
	[%
		TOKENS = {
			home_breadcrumb = "Etusivu"
			your_payment_breadcrumb = "Your Payment"
			error = "Virhe"
			error_title = "maksun käsittelyssä tapahtui virhe"
			PAYPAL_UNABLE_TO_CONNECT = "PayPaliin ei saada yhteyttä."
			PAYPAL_TRY_LATER = "Yritä myöhemmin uudelleen."
			PAYPAL_ERROR_PROCESSING = "Maksun vahvistaminen ei onnistu."
			PAYPAL_CONTACT_LIBRARY = "Ota yhteys kirjastoon maksun vahvistamiseksi."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Palaa maksutietoihin"
		}
	%]
*/