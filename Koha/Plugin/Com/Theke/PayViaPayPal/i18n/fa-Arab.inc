/*
	[%
		TOKENS = {
			home_breadcrumb = "صفحه اصلی"
			your_payment_breadcrumb = "Your Payment"
			error = "خطا"
			error_title = "مشکلی در تأئید شما وجود دارد"
			PAYPAL_UNABLE_TO_CONNECT = "عدم امکان افزودن یک یا بیش از یک برچسب."
			PAYPAL_TRY_LATER = "لطفاً بعد دوباره تلاش کنید."
			PAYPAL_ERROR_PROCESSING = "عدم امکان افزودن یک یا بیش از یک برچسب."
			PAYPAL_CONTACT_LIBRARY = "لطفاً برای تصدیق پرداخت، با کتابخانه تماس بگیرید."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "العودة إلى:"
		}
	%]
*/