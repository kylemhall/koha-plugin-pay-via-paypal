/*
	[%
		TOKENS = {
			home_breadcrumb = "Start"
			your_payment_breadcrumb = "Your Payment"
			error = "Fejl:"
			error_title = "Der opstod et problem med din indtastning"
			PAYPAL_UNABLE_TO_CONNECT = "Kunne ikke tilføje et eller flere mærker."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "Kunne ikke tilføje et eller flere mærker."
			PAYPAL_CONTACT_LIBRARY = ". Kontakt biblioteket, for mere information."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Returner dette emne "
		}
	%]
*/