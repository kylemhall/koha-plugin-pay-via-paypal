/*
	[%
		TOKENS = {
			home_breadcrumb = "Accueil"
			your_payment_breadcrumb = "Your Payment"
			error = "Erreur"
			error_title = "Une erreur est survenue pendant l'exécution de votre payment"
			PAYPAL_UNABLE_TO_CONNECT = "Impossible de se connecter à PayPal."
			PAYPAL_TRY_LATER = "Merci de réessayer ultérieurement."
			PAYPAL_ERROR_PROCESSING = "Impossible de vérifier le paiement."
			PAYPAL_CONTACT_LIBRARY = "Veuillez contacter la bibliothèque pour vérifier votre paiment."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Revenir à la page des amendes et frais"
		}
	%]
*/