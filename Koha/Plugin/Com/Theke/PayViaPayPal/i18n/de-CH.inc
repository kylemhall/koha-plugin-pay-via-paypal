/*
	[%
		TOKENS = {
			home_breadcrumb = "Start"
			your_payment_breadcrumb = "Your Payment"
			error = "Fehler"
			error_title = "es gab ein Problem bei der Durchführung Ihrer Zahlung"
			PAYPAL_UNABLE_TO_CONNECT = "Keine Verbindung zu PayPal."
			PAYPAL_TRY_LATER = "Bitte versuchen Sie es später noch einmal."
			PAYPAL_ERROR_PROCESSING = "Zahlung kann nicht verifizeirt werden."
			PAYPAL_CONTACT_LIBRARY = "Bitte kontaktieren Sie Ihre Bibliothek um Ihre Zahlung zu überprüfen."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Zurück zu den Gebühren-Details"
		}
	%]
*/