/*
	[%
		TOKENS = {
			home_breadcrumb = "الصفحة الرئيسية"
			your_payment_breadcrumb = "Your Payment"
			error = "خطأ:"
			error_title = "هــناك مشكلة خلال معالجة دفعتك"
			PAYPAL_UNABLE_TO_CONNECT = "غير قادر على إلاتصال بـ PayPal."
			PAYPAL_TRY_LATER = "فضلاً حاول مرة أخرى لاحقًا."
			PAYPAL_ERROR_PROCESSING = "غير قادر على التحقق من دفعك."
			PAYPAL_CONTACT_LIBRARY = "الرجاء التواصل مع مكتبتك للتأكد من وصول دفعتك."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "ارجع الى تفاصيل الغرامة"
		}
	%]
*/