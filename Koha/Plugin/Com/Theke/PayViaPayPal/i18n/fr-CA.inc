/*
	[%
		TOKENS = {
			home_breadcrumb = "Accueil"
			your_payment_breadcrumb = "Your Payment"
			error = "Erreur"
			error_title = "Il y a eu un problème avec votre paiement"
			PAYPAL_UNABLE_TO_CONNECT = "Impossible de se connecter à PayPal."
			PAYPAL_TRY_LATER = "Merci de réessayer plus tard."
			PAYPAL_ERROR_PROCESSING = "Impossible de vérifier le paiement."
			PAYPAL_CONTACT_LIBRARY = "Veuillez contacter votre bibliothèque pour vérifier votre paiement."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Retourner aux détails des amendes"
		}
	%]
*/